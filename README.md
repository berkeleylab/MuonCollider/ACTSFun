# ACTS Fun
Code to play around with using ACTS for tracking at the imaginary muon collider detector.

## Requirements

### Geantino Scan Only
- [Geant4](https://geant4.web.cern.ch/)
- [DD4HEP](https://github.com/AIDASoft/DD4hep)
- [ACTS](https://github.com/acts-project/acts)
- [lcgeo](https://github.com/MuonColliderSoft/lcgeo) (tag [v00-16-08-MC](https://github.com/MuonColliderSoft/lcgeo/tree/v00-16-08-MC) from MuonColliderSoft required)

### Ploting
- [Python 3](https://www.python.org/)
- [pandas](https://pandas.pydata.org/)
- [uproot](https://github.com/scikit-hep/uproot4)
- [matplotlib](https://matplotlib.org/)


## Setup Instructions

### Container
All commands should be run inside the `gitlab-registry.cern.ch/berkeleylab/muoncollider/muoncollider-docker/mucoll-ilc-framework:1.4.0-centos8` container.

### Build Instructions

The following instructions assume that one is running inside the `gitlab-registry.cern.ch/berkeleylab/muoncollider/muoncollider-docker/mucoll-ilc-framework:1.4.0-centos8`. The search paths should be modified for other environments.

```bash
source /opt/ilcsoft/muonc/init_ilcsoft.sh
cmake -S . -B build/ -DBOOST_INCLUDEDIR=/usr/include/boost169 -DBOOST_LIBRARYDIR=/usr/lib64/boost169
cmake --build build/
```

### Setup Script
The included `setup.sh` script is useful for defining all paths for the binaries built by the workspace. At the current stage, it setups the following:

- ILC software via `init_ilcsoft.sh`
- External binaries/libraries found in `exts`
- `MYBUILD` enviromental variable pointing to the build directory
- `MYWORKSPACE` enviromental variable pointing to the clone of this repository

Run the following at the start of every session. It has an optional argument to the build directory and is set to `build/` by default.

```shell
source setup.sh [build]
```

## Repository Structure
- `exts/` Custom external packages.
- `response/` Common response files for use with ACTS examples.
- `python/` Scripts for plotting results.

## Example Commands

### Simulated Hits
```shell
${MYBUILD}/exts/acts/bin/ActsExampleFatrasTGeo --response-file ${MYBUILD}/response/tgeo-lctracker.response --output-root 1 --events 1000
```

### Truth Tracking
```shell
${MYBUILD}/exts/acts/bin/ActsExampleFatrasTGeo --response-file ${MYBUILD}/response/tgeo-mat-lctracker.response --bf-values 0 0 3.57 --output-dir OUT_mu --output-csv 1 -n 1000
${MYBUILD}/exts/acts/bin/ActsExampleTruthTracksTGeo --response-file ${MYBUILD}/response/tgeo-mat-lctracker.response --bf-values 0 0 3.57 -n 1000
```

## ACTS Modifications
This package includes a modified version of ACTS to handle loading of the MCC geometry. The modifications are:

- 0.1 mm (used to be 1.0 mm) envelope around layers. This is needed for the doublet layers in the vertex.

### ACTS Build Settings
The following optional ACTS components are automatically enabled.
- `ACTS_BUILD_FATRAS`
- `ACTS_BUILD_PLUGIN_TGEO`
- `ACTS_BUILD_PLUGIN_DD4HEP`
- `ACTS_BUILD_EXAMPLES`
- `ACTS_BUILD_EXAMPLES_DD4HEP`
- `ACTS_BUILD_EXAMPLES_TGEO`
- `ACTS_BUILD_EXAMPLES_GEANT4`

