import sys

import pandas as pd
import numpy as np

# kVolumeMask    = 0xff00000000000000
# kBoundaryMask  = 0x00ff000000000000
# kLayerMask     = 0x0000fff000000000
# kApproachMask  = 0x0000000ff0000000
# kSensitiveMask = 0x000000000fffffff

if len(sys.argv)!=3:
    print('usage: {} input.csv output.csv'.format(sys.argv[0]))
    sys.exit(1)

df=pd.read_csv(sys.argv[1])
df['tr']=np.sqrt(df.tx*df.tx+df.ty*df.ty)

df['volume_id' ]=np.right_shift(df.geometry_id,56)&0xFF
df['layer_id'  ]=np.right_shift(df.geometry_id,36)&0xFFF
df['sensor_id' ]=np.right_shift(df.geometry_id, 0)&0xFFFFFFF

df.sort_values('tr')[['tr','tx','ty','tz','geometry_id','volume_id','layer_id','sensor_id']].to_csv(sys.argv[2], index=False)
